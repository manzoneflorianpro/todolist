<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}


$user = new DAO($db, 'user');
$todo = new DAO($db, 'todo');

$category = valid_donnees($_POST['category']);
$title = valid_donnees($_POST['title']);
$description = valid_donnees($_POST['description']);
$status = valid_donnees($_POST['status']);
$date_limite = valid_donnees($_POST['date_limite']);
$user_id = $_SESSION['id'];
$date = date('Y-m-d');

if (!empty($category) && !empty($title) && !empty($description) && !empty($status)) {
    $datas = array(
        'categorie' => $category,
        'titre' => $title,
        'description' => $description,
        'status' => $status,
        'cree_le' => $date,
        'date_limite' => $date_limite,
        'id_user' => $user_id
    );

    $todo->create($datas);
    header('location: ../page2.php');
}
