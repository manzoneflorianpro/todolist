<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (!isset($_SESSION['pseudo'])) {
    header('location: ../index.php');
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user = new DAO($db, 'user');
$todo = new DAO($db, 'todo');

if (isset($_GET['id'])) {
    $id_todo = $_GET['id'];

    $todo_to_delete = $todo->find_by('id_todo', $id_todo);

    if ($todo_to_delete['id_user'] == $_SESSION['id']) {
        $todo->delete('id_todo', $id_todo);
        header('location: ../page2.php');
    } else {
        header('location: ../page2.php');
    }
} else {
    header('location: ../page2.php');
}
