<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: ../index.php');
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user = new DAO($db, 'user');
$todo = new DAO($db, 'todo');

if (isset($_GET['id'])) {

    $id_todo = $_GET['id'];

    $todo_to_update = $todo->find_by('id_todo', $id_todo);


    if (gettype($todo_to_update) == 'array' && $todo_to_update['id_user'] == $_SESSION['id']) {
        $categorie = $todo_to_update['categorie'];
        $title = $todo_to_update['titre'];
        $description = $todo_to_update['description'];
        $date = $todo_to_update['date_limite'];
        $status = $todo_to_update['status'];
    } else {
        header('location: ../page2.php');
    }
} else {
    if (session_status() !== PHP_SESSION_ACTIVE) {
        header('location: ../index.php');
    } else {
        header('location: ../page2.php');
    }
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/main.css">
    <title>Update todo</title>
</head>

<body>
    <form action="../traitement/traitementupdate.php?id_todo= <?php echo $id_todo ?>" method="POST">
        <fieldset>
            <legend>UPDATE</legend>
            <input type="text" name="category" value="<?php echo $categorie ?>" disabled>
            <br />
            <label for="title">Titre</label>
            <input type="text" name="title" value="<?php echo $title ?>" required>
            <br />
            <label for="description">Description</label>
            <textarea name="description" required><?php echo $description ?></textarea>
            <br />
            <label for="status">Statut :</label>
            <?php
            switch ($status) {
                case 'À faire':
            ?>
                    <select name="status" required>
                        <option value="À faire" selected>À faire</option>
                        <option value="En cours">En cours</option>
                        <option value="Fait">Fait</option>
                    </select>
                <?php
                    break;
                case 'En cours':
                ?>
                    <select name="status" required>
                        <option value="À faire">À faire</option>
                        <option value="En cours" selected>En cours</option>
                        <option value="Fait">Fait</option>
                    </select>
                <?php
                    break;
                case 'Fait':
                ?>
                    <select name="status" required>
                        <option value="À faire">À faire</option>
                        <option value="En cours">En cours</option>
                        <option value="Fait" selected>Fait</option>
                    </select>
            <?php
                    break;
            }
            ?>
            <br />
            <label for="date_limite">À faire avant :</label>
            <input type="date" name="date_limite" value="<?php echo $date ?>">
            <input type="submit" value="Modifier">
            <a href="page2.php" id="annulation">Annuler</a>
        </fieldset>
    </form>
</body>

</html>