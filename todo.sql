CREATE DATABASE todolist;

use todolist;

CREATE TABLE user (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL UNIQUE,
    pseudo VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE todo (
    id_todo INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    cree_le DATE NOT NULL,
    date_limite DATE,
    status VARCHAR(50) NOT NULL,
    categorie VARCHAR(50) NOT NULL,
    id_user INT NOT NULL
);

CREATE USER todolist@localhost IDENTIFIED BY 'todolist123'; 
GRANT ALL ON todolist.* TO todolist@localhost; 
FLUSH PRIVILEGES; 