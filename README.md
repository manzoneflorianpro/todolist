# Exemple fichier config.php

```PHP
/**
 * Nettoie les données du formulaire
 * @param Mixed $donnees Les données à nettoyer
 * @return Mixed Les données nettoyées
 */
function valid_donnees($donnees)
{
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    return $donnees;
}


define('DSN', 'mysql:host=localhost;dbname="DBNAME";charset=utf8');
define('DB_USER', 'USER_NAME');
define('DB_PASS', 'USER_PASSWORD');
```

# Organisation du code

> assets/css  
> CRUD/create.php | delete.php | update.php  
> PDO/functions.php  
> traitement/traitementsignin.php | traitementsignup.php | traitementupdate.php

index.php contient 2 formulaires (Inscription et Connexion)  
page2.php contient une liste de toutes les todos du user, la possibilité des les mettre à jour, les supprimer ou en ajouter et la possiblité de se déconnecter grâce à deconnexion.php.

# Fonctionalités

> **Création** de compte, **connexion** et **déconnexion**.  
> Accès à la **liste de taches**, **ajout**, **modifications** et **suppression** de tâches.

# Ressources

> PHP.net  
> SQL.sh
