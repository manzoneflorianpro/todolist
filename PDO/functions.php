<?php

class DAO
{
    /**
     * @param PDO $database La DB sur laquelle on travaille
     * @param String $table
     * @return Void
     */
    function __construct($database, $table)
    {
        $this->database = $database;
        $this->table = $table;
    }

    /**
     * Récupere une donnée précise
     * 
     * @param String $field est le champ utilisé pour trouver notre donnée
     * @param Mixed $value est la valeur que doit avoir le champ de la donnée
     * @return Array La donnée demandée
     */
    function find_by($field, $value)
    {
        $query = "SELECT * FROM {$this->table} WHERE {$field} = :value";
        $statement = $this->database->prepare($query);
        $statement->execute(array('value' => $value));
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Récupere toutes les données d'une table
     * 
     * @return Array
     */
    function find_all($session_id)
    {
        $query = "SELECT * FROM {$this->table} WHERE id_user = :id_user";
        $statement = $this->database->prepare($query);
        $statement->bindValue(':id_user', $session_id);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Cette fonction permet d'insérer une nouvelle ligne dans une table.
     * 
     * @param Array  $datas Un tableau associatif, dont les clés correspondent impérativement aux noms des colonnes de la table dans laquelle on insère les données
     * @return Void
     */
    function create($datas)
    {
        $requeteSQL = "INSERT INTO {$this->table}(";
        $colonnesNames = array_keys($datas);
        $colonnes = implode(', ', $colonnesNames);
        $requeteSQL .= "{$colonnes})";
        $requeteSQL .= ' VALUES (:';
        $colonnes = implode(', :', $colonnesNames);
        $requeteSQL .= "{$colonnes})";

        $requetePreparee = $this->database->prepare($requeteSQL);
        foreach ($datas as $key => $value) {
            $requetePreparee->bindValue($key, $value);
        }
        $requetePreparee->execute();
    }

    /**
     * Efface une donnée précise 
     * 
     * @param String $field Champ qui nous sert pour rechercher la donnée
     * @param Mixed $value_field Valeur du champ pour trouver la donnée à effacer
     * @return Void
     */
    function delete($field = 'id', $value_field)
    {
        $query = "DELETE FROM {$this->table} WHERE {$field} = :value_field";
        $statement = $this->database->prepare($query);
        $statement->execute(array('value_field' => $value_field));
    }

    /**
     * Cette fonction permet de mettre à jour une ligne (enregistrement) dans une table.
     *
     * @param Array  $datas un tableau associatif, dont les clés correspondent impérativement aux noms des colonnes de la table dans laquelle on insère les données
     * @param String $field   nom de la clé primaire
     * @param Mixed  $value    int ou string représentant l'id de l'élément à mettre à jour
     */
    function update($datas, $field, $value)
    {
        $sql = "UPDATE {$this->table} SET ";
        $updated_fields = [];
        foreach ($datas as $column => $val) {
            $key_value_pair = "{$column} = :{$column}";
            array_push($updated_fields, $key_value_pair);
        }
        $sql .= implode(', ', $updated_fields);
        $sql .= " WHERE {$field} = :id";
        $requetePreparee = $this->database->prepare($sql);
        $requetePreparee->bindValue('id', $value);
        foreach ($datas as $key => $val) {
            $requetePreparee->bindValue($key, $val);
        }
        $requetePreparee->execute();
    }
}
