<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
} else {
    header('location: ../page2.php');
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user_functions = new DAO($db, 'user');

$pseudo = valid_donnees($_POST['pseudo']);
$password = valid_donnees($_POST['password']);

if (!empty($pseudo)) {

    try {
        $user = $user_functions->find_by('pseudo', $pseudo);
        if (!empty($user) && password_verify($password, $user['password'])) {
            session_start();

            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['id'] = $user['id'];

            header('location: ../page2.php');
        } else {
            header('Location: ../index.php');
        }
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
} else {
    header('Location: ../index.php');
}
