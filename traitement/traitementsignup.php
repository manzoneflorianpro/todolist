<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user = new DAO($db, 'user');

$email = valid_donnees($_POST['email']);
$pseudo = valid_donnees($_POST['pseudo']);
$password = valid_donnees($_POST['password']);
$password_confirmed = valid_donnees($_POST['confirmed_password']);

filter_input(INPUT_POST, $email, FILTER_SANITIZE_EMAIL);

if (!empty($pseudo) && !empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {


    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    $datas = array('email' => $email, 'pseudo' => $pseudo, 'password' => $hashed_password);
    try {
        $user->create($datas);
        header("Location: ../index.php");
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
} else {
    header('Location: ../index.php');
}
