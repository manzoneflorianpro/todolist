<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../config.php';
require_once '../PDO/functions.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


$user = new DAO($db, 'user');
$todo = new DAO($db, 'todo');

$title = valid_donnees($_POST['title']);
$description = valid_donnees($_POST['description']);
$status = valid_donnees($_POST['status']);
$date_limite = valid_donnees($_POST['date_limite']);
$id_todo = $_GET['id_todo'];


if (!empty($title) && !empty($description) && !empty($status)) {
    $datas = array(
        'titre' => $title,
        'description' => $description,
        'status' => $status,
        'date_limite' => $date_limite,
    );
    $todo->update($datas, 'id_todo', $id_todo);
    header('location: ../page2.php');
}else {
    header('location: ../CRUD/update.php');
}
