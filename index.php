<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/main.css">
    <title>ToDo List</title>
</head>

<body>
    <section>
        <form action="traitement/traitementsignup.php" method="post">
            <fieldset>
                <legend>INSCRIPTION</legend>
                <input type="email" name="email" placeholder="Email" required>
                <input type="text" name="pseudo" placeholder="Pseudo" required>
                <input type="password" name="password" placeholder="Mot de passe" required>
                <input type="password" name="confirmed_password" placeholder="Confirmation du mot de passe" required>
                <input type="submit" value="Envoyer">
            </fieldset>
        </form>
    </section>
    <section>
        <form action="traitement/traitementsignin.php" method="post" class="connect">
            <fieldset>
                <legend>CONNEXION</legend>
                <input type="text" name="pseudo" placeholder="Pseudo" required>
                <input type="password" name="password" placeholder="Mot de passe" required>
                <input type="submit" value="Envoyer">
            </fieldset>
        </form>
    </section>
    <?php
    // if (session_status() !== PHP_SESSION_ACTIVE) {
    //     session_start();
    // }
    ?>
</body>

</html>