<?php

require_once 'config.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: index.php');
}

require_once 'PDO/functions.php';


$user_functions = new DAO($db, 'user');
$todo_functions = new DAO($db, 'todo');
$id_session = $_SESSION['id'];

$all_todos = $todo_functions->find_all($id_session);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css">
    <link rel="stylesheet" href="assets/css/page2.css">
    <title>ToDo List</title>
</head>

<body>
    <header>
        <a href="deconnexion.php">Déconnexion</a>
    </header>
    <main>
        <p class="add"><a href="#open-modal"><i class="fa-solid fa-plus"></i> ToDo</a></p>
        <div id="open-modal" class="modal-window">
            <div>
                <a href="#" title="Close" class="modal-close">Close</a>
                <form action="CRUD/create.php" method="post">
                    <label for="category">Categorie</label>
                    <input type="text" name="category" placeholder="Category">
                    <label for="title">Titre</label>
                    <input type="text" name="title" placeholder="Title">
                    <label for="description">Description</label>
                    <input type="text" name="description" placeholder="Description">
                    <label for="status">Statut :</label>
                    <select name="status" required>
                        <option value="À faire" selected>À faire</option>
                        <option value="En cours">En cours</option>
                        <option value="Fait">Fait</option>
                    </select><br/><br/>
                    <label for="date_limite">À faire avant :</label>
                    <input type="date" name="date_limite">
                    <input type="submit" value="Ajouter">
                </form>

            </div>
        </div>
        <section class="todolist">
            <?php
            if (empty($all_todos)) {
                echo 'La liste des choses à faire est vide';
            } else {
                for ($i = 0; $i < count($all_todos); $i++) {
            ?>
                    <article class="todo">
                        <p><?php print_r($all_todos[$i]['titre']) ?></p>
                        <p><?php print_r($all_todos[$i]['description']) ?></p>
                        <p><?php print_r($all_todos[$i]['status']) ?></p>
                        <p>Fin : <?php print_r($all_todos[$i]['date_limite']) ?></p>
                        <p>
                            <a href="CRUD/update.php?id=<?php echo $all_todos[$i]['id_todo'] ?>">
                                <i class="fa-solid fa-pencil"></i>
                            </a>
                            <a href="CRUD/delete.php?id=<?php echo $all_todos[$i]['id_todo'] ?>">
                                <i class="fa-solid fa-trash-can"></i>
                            </a>
                        </p>
                    </article>
            <?php
                }
            }
            ?>
        </section>
    </main>
</body>

</html>